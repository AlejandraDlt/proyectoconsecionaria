/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo.proyecto.panel;

import java.util.ArrayList;
import poo.proyecto.clase.Auto;
import poo.proyecto.clase.Camioneta;
import poo.proyecto.clase.Cliente;
import poo.proyecto.clase.Moto;
import poo.proyecto.clase.Vehiculo;

/**
 *
 * @author alexa
 */
public class PnlRegistroVenta extends javax.swing.JPanel {

    Cliente c1;
    Vehiculo v1;
    static ArrayList <Auto> listaAutosVendidos ;
    static ArrayList <Moto> listaMotosVendidos;
    static ArrayList <Camioneta> listaCamionetasVendidos;
    static ArrayList <Cliente> listaVentas;
    
    public PnlRegistroVenta() {
        initComponents();
        cmbTransmision.setVisible(false);
        cmbTipoCabina.setVisible(false);
        cmbTipoMoto.setVisible(false);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlVenta = new javax.swing.JPanel();
        lblMarca = new javax.swing.JLabel();
        lblModelo = new javax.swing.JLabel();
        lblAnio = new javax.swing.JLabel();
        txtMarca = new javax.swing.JTextField();
        txtModelo = new javax.swing.JTextField();
        txtAnio = new javax.swing.JTextField();
        lblVehiculo = new javax.swing.JLabel();
        cmbVehiculo = new javax.swing.JComboBox<>();
        btnVender = new javax.swing.JToggleButton();
        lblPrecio = new javax.swing.JLabel();
        txtPrecio = new javax.swing.JTextField();
        pnlCaracteristicas = new javax.swing.JPanel();
        lblTipoMoto = new javax.swing.JLabel();
        lblTransmision = new javax.swing.JLabel();
        cmbTransmision = new javax.swing.JComboBox<>();
        cmbTipoMoto = new javax.swing.JComboBox<>();
        cmbTipoCabina = new javax.swing.JComboBox<>();
        lblTipoCabina = new javax.swing.JLabel();

        pnlVenta.setBorder(javax.swing.BorderFactory.createTitledBorder("Venta"));

        lblMarca.setText("Marca");

        lblModelo.setText("Modelo");

        lblAnio.setText("Año");

        txtAnio.setToolTipText("");

        lblVehiculo.setText("Vehículo");

        cmbVehiculo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Auto", "Moto", "Camioneta" }));
        cmbVehiculo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbVehiculoActionPerformed(evt);
            }
        });

        btnVender.setText("Vender");
        btnVender.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVenderActionPerformed(evt);
            }
        });

        lblPrecio.setText("Precio");

        txtPrecio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPrecioActionPerformed(evt);
            }
        });

        pnlCaracteristicas.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        lblTipoMoto.setText("Tipo de Moto");

        lblTransmision.setText("Transmision");
        lblTransmision.setToolTipText("");

        cmbTransmision.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Manual", "Automatica" }));

        cmbTipoMoto.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Deportiva", "Motocross", "Electrica", "Ciclomotores" }));

        cmbTipoCabina.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Simple", "Doble" }));

        lblTipoCabina.setText("Tipo de Cabina");

        javax.swing.GroupLayout pnlCaracteristicasLayout = new javax.swing.GroupLayout(pnlCaracteristicas);
        pnlCaracteristicas.setLayout(pnlCaracteristicasLayout);
        pnlCaracteristicasLayout.setHorizontalGroup(
            pnlCaracteristicasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlCaracteristicasLayout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(pnlCaracteristicasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(pnlCaracteristicasLayout.createSequentialGroup()
                        .addComponent(lblTransmision)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(cmbTransmision, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnlCaracteristicasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(pnlCaracteristicasLayout.createSequentialGroup()
                            .addComponent(lblTipoCabina)
                            .addGap(18, 18, 18)
                            .addComponent(cmbTipoCabina, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(pnlCaracteristicasLayout.createSequentialGroup()
                            .addComponent(lblTipoMoto)
                            .addGap(25, 25, 25)
                            .addComponent(cmbTipoMoto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(32, 32, 32))
        );
        pnlCaracteristicasLayout.setVerticalGroup(
            pnlCaracteristicasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlCaracteristicasLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlCaracteristicasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblTransmision)
                    .addComponent(cmbTransmision, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlCaracteristicasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbTipoMoto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblTipoMoto, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(pnlCaracteristicasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblTipoCabina)
                    .addComponent(cmbTipoCabina, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout pnlVentaLayout = new javax.swing.GroupLayout(pnlVenta);
        pnlVenta.setLayout(pnlVentaLayout);
        pnlVentaLayout.setHorizontalGroup(
            pnlVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlVentaLayout.createSequentialGroup()
                .addContainerGap(30, Short.MAX_VALUE)
                .addGroup(pnlVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnlCaracteristicas, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(pnlVentaLayout.createSequentialGroup()
                        .addComponent(lblVehiculo)
                        .addGap(34, 34, 34)
                        .addComponent(cmbVehiculo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnlVentaLayout.createSequentialGroup()
                        .addGroup(pnlVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblMarca)
                            .addComponent(lblModelo)
                            .addComponent(lblAnio)
                            .addComponent(lblPrecio))
                        .addGap(39, 39, 39)
                        .addGroup(pnlVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnlVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(txtMarca)
                                .addComponent(txtModelo)
                                .addComponent(txtAnio, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(pnlVentaLayout.createSequentialGroup()
                                .addComponent(txtPrecio, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 105, Short.MAX_VALUE)
                                .addComponent(btnVender)))))
                .addGap(48, 48, 48))
        );
        pnlVentaLayout.setVerticalGroup(
            pnlVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlVentaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblVehiculo)
                    .addComponent(cmbVehiculo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(pnlCaracteristicas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(pnlVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblMarca)
                    .addComponent(txtMarca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(pnlVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblModelo)
                    .addComponent(txtModelo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(pnlVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblAnio)
                    .addComponent(txtAnio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(28, 28, 28)
                .addGroup(pnlVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtPrecio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblPrecio)
                    .addComponent(btnVender))
                .addContainerGap(28, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnlVenta, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnlVenta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void txtPrecioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPrecioActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPrecioActionPerformed

    private void btnVenderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVenderActionPerformed
        String marca = txtMarca.getText();
        String modelo = txtModelo.getText();
        int anio = Integer.parseInt(txtAnio.getText());
        String vehiculo = cmbVehiculo.getSelectedItem().toString();
        int precio = Integer.parseInt(txtPrecio.getText());

        if (vehiculo.equals("Moto")) {
            String tipoMoto = cmbTipoMoto.getSelectedItem().toString();
            v1 = new Moto(modelo, marca, anio,precio, tipoMoto);
            listaMotosVendidos.add((Moto) v1);            
        }
        if (vehiculo.equals("Auto")) {
            String tipoTransmision = cmbTransmision.getSelectedItem().toString();
            v1 = new Auto(modelo, marca, anio,precio, tipoTransmision);
            listaAutosVendidos.add((Auto) v1);
        }
        if (vehiculo.equals("Camioneta")) {
            String tipoCabina = cmbTipoCabina.getSelectedItem().toString();
            v1 = new Camioneta(modelo, marca, anio,precio, tipoCabina);
            listaCamionetasVendidos.add((Camioneta) v1);
        }
        
        Cliente c = GUIMenuPrincipal.clientes.get(GUIMenuPrincipal.clientes.size()-1);
        c.setVehiculoCliente(v1);
        
    }//GEN-LAST:event_btnVenderActionPerformed

    private void cmbVehiculoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbVehiculoActionPerformed
        if (cmbVehiculo.getSelectedIndex()==0){
            cmbTransmision.setVisible(true);
            cmbTipoCabina.setVisible(false);
            cmbTipoMoto.setVisible(false);
            
        }
        if (cmbVehiculo.getSelectedIndex()==2){
            cmbTipoCabina.setVisible(true);
            cmbTransmision.setVisible(false);
            cmbTipoMoto.setVisible(false);
        }
        if (cmbVehiculo.getSelectedIndex()==1){
            cmbTipoMoto.setVisible(true);
            cmbTransmision.setVisible(false);
            cmbTipoCabina.setVisible(false);
        }
    }//GEN-LAST:event_cmbVehiculoActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JToggleButton btnVender;
    private javax.swing.JComboBox<String> cmbTipoCabina;
    private javax.swing.JComboBox<String> cmbTipoMoto;
    private javax.swing.JComboBox<String> cmbTransmision;
    private javax.swing.JComboBox<String> cmbVehiculo;
    private javax.swing.JLabel lblAnio;
    private javax.swing.JLabel lblMarca;
    private javax.swing.JLabel lblModelo;
    private javax.swing.JLabel lblPrecio;
    private javax.swing.JLabel lblTipoCabina;
    private javax.swing.JLabel lblTipoMoto;
    private javax.swing.JLabel lblTransmision;
    private javax.swing.JLabel lblVehiculo;
    private javax.swing.JPanel pnlCaracteristicas;
    private javax.swing.JPanel pnlVenta;
    private javax.swing.JTextField txtAnio;
    private javax.swing.JTextField txtMarca;
    private javax.swing.JTextField txtModelo;
    private javax.swing.JTextField txtPrecio;
    // End of variables declaration//GEN-END:variables
}
