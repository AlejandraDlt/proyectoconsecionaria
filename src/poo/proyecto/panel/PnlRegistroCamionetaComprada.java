/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo.proyecto.panel;

import javax.swing.table.DefaultTableModel;
import poo.proyecto.clase.Camioneta;
/**
 *
 * @author Alejandra
 */
public class PnlRegistroCamionetaComprada extends javax.swing.JPanel {
    DefaultTableModel dtmModelo;
    /**
     * Creates new form PnlRegistroCompraAuto
     */
    public PnlRegistroCamionetaComprada() {
        initComponents();
        dtmModelo = new DefaultTableModel();
        dtmModelo.addColumn("Modelo");
        dtmModelo.addColumn("Marca");
        dtmModelo.addColumn("Año");
        tblRegistroCamionetaComprada.setModel(dtmModelo);
    }
    public void llenarTabla(){
        dtmModelo.setRowCount(0);
        for(Camioneta c:  GUIMenuPrincipal.camionetas){
            dtmModelo.addRow(new Object[]{c.getModelo(),c.getMarca(),c.getAño()});
        }
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tblRegistroCamionetaComprada = new javax.swing.JTable();

        setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Registro de Camionetas Compradas", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.BELOW_TOP, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        tblRegistroCamionetaComprada.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblRegistroCamionetaComprada);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 375, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 246, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(29, 29, 29))
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblRegistroCamionetaComprada;
    // End of variables declaration//GEN-END:variables
}
