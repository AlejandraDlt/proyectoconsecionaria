
package poo.proyecto.clase;

/**
 *
 * @author Alejandra
 */
public class Cliente {
    private String nombre;
    private String apellido;
    private int cedula;
    private int celular;
    private Vehiculo vehiculoCliente;
    
    public Cliente(String nombre, String apellido, int cedula, int celular, Vehiculo vehiculoCliente){
        this.nombre = nombre;
        this.apellido = apellido;
        this.cedula = cedula;
        this.celular = celular;
        this.vehiculoCliente = vehiculoCliente;
    }

    public String getNombre() {
        return nombre;
    }

    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public int getCedula() {
        return cedula;
    }

    public void setCedula(int cedula) {
        this.cedula = cedula;
    }

    public int getCelular() {
        return celular;
    }

    public void setCelular(int celular) {
        this.celular = celular;
    }

    public Vehiculo getVehiculoCliente() {
        return vehiculoCliente;
    }

    public void setVehiculoCliente(Vehiculo vehiculoCliente) {
        this.vehiculoCliente = vehiculoCliente;
    }
    
}
