/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo.proyecto.clase;

/**
 *
 * @author Alejandra
 */
public class Moto extends Vehiculo implements Comparable <Moto>{
    private String tipoMoto;
    
    public Moto(String modelo, String marca, int año, double precio, String tipoMoto){
        super(modelo,marca,año, precio);
        this.tipoMoto = tipoMoto;
    }

    public String getTipoMoto() {
        return tipoMoto;
    }

    public void setTipoMoto(String tipoMoto) {
        this.tipoMoto = tipoMoto;
    }
    @Override
    public int compareTo(Moto m1) {
        
        if(this.año < m1.getAño())
            return -1;
        
        if(this.año < m1.getAño())
            return  1;
        
        
            return 0;
    }
}
