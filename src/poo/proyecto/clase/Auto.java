 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo.proyecto.clase;

/**
 *
 * @author Alejandra
 */
public class Auto extends Vehiculo implements Comparable <Auto>{
    private String transmision;
    
    public Auto(String modelo, String marca, int año, double precio, String transmision){
        super(modelo, marca, año, precio);
        this.transmision = transmision;
    }

    public String getTransmision() {
        return transmision;
    }

    public void setTransmision(String transmision) {
        this.transmision = transmision;
    }
    @Override
    public int compareTo(Auto a1) {
        
        if(this.año < a1.getAño())
            return -1;
        
        if(this.año < a1.getAño())
            return  1;
        
        
            return 0;
    }
}
