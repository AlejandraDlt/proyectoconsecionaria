/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo.proyecto.clase;

/**
 *
 * @author Alejandra
 */
public class Camioneta extends Vehiculo implements Comparable <Camioneta>{
    private String tipoCabina;
    
    public Camioneta(String modelo, String marca, int año,double precio, String tipoCabina){
        super(modelo, marca, año, precio);
        this.tipoCabina = tipoCabina;
    }

    public String getTipoCabina() {
        return tipoCabina;
    }

    public void setTipoCabina(String tipoCabina) {
        this.tipoCabina = tipoCabina;
    }
    
    @Override
    public int compareTo(Camioneta c1) {
        
        if(this.año < c1.getAño())
            return -1;
        
        if(this.año < c1.getAño())
            return  1;
        
        
            return 0;
    }
}
