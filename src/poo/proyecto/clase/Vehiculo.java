/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo.proyecto.clase;

/**
 *
 * @author Alejandra
 */
public class Vehiculo {

    protected String modelo;
    protected String marca;
    protected int año;
    private double precio;

    public Vehiculo(String modelo, String marca, int año, double precio) {
        this.modelo = modelo;
        this.marca = marca;
        this.año = año;
        this.precio = precio;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public int getAño() {
        return año;
    }

    public void setAño(int año) {
        this.año = año;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

}
