/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo.proyecto.panel;

import java.util.Iterator;
import javax.swing.table.DefaultTableModel;
import poo.proyecto.clase.Auto;
import poo.proyecto.clase.Moto;

/**
 *
 * @author User
 */
public class PnlRegistroVentaAuto extends javax.swing.JPanel {
    DefaultTableModel dtmModelo;

    /**
     * Creates new form PnlRegistroCompraAuto
     */
    public PnlRegistroVentaAuto() {
        initComponents();
        dtmModelo = new DefaultTableModel();
        dtmModelo.addColumn("Modelo");
        dtmModelo.addColumn("Marca");
        dtmModelo.addColumn("Año");
        tblRegistroMotoVendida.setModel(dtmModelo);
    }
    
    public void llenarTabla(){
        dtmModelo.setRowCount(0);
        for (Auto a : PnlRegistroVenta.listaAutosVendidos) {
            dtmModelo.addRow(new Object[]{a.getModelo(),a.getMarca(),a.getAño()});
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tblRegistroMotoVendida = new javax.swing.JTable();

        tblRegistroMotoVendida.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblRegistroMotoVendida);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(15, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 375, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(25, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 246, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(29, 29, 29))
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblRegistroMotoVendida;
    // End of variables declaration//GEN-END:variables
}
